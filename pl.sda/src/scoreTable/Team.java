package scoreTable;

/**
 * 19.10.2018
 **/
public class Team {
    private String nameTeam;

    public Team(String nameTeam) {
        this.nameTeam = nameTeam;
    }

    public String getNameTeam() {
        return nameTeam;
    }

    public void setNameTeam(String nameTeam) {
        this.nameTeam = nameTeam;
    }
}
